#!/usr/bin/env python
# coding: utf-8

import pandas as pd
import sys
import os

org=sys.argv[1]
print(str(org))

#reading our merged GTF
main_df = pd.read_csv('./'+ str(org) +  '/merged.gtf', sep="\t", header=None)
# Find transcript_ID
main_df["transcript"]= main_df[8].str.split('; ', n=2, expand=True)[1].str.split(' ', n=2, expand=True)[1].str.replace('"', '')
# Leave only scaff and transcript_ID
main_df = main_df[[0, "transcript"]]
#Drop duplicated obs
main_df.drop_duplicates(subset ="transcript", keep = "first", inplace = True) 
main_df.shape

experiments=[]
with open('./'+ str(org) + '/GTFs.txt') as file:
    for line in file:
        experiments.append(line.strip().split('/')[3].split('_')[0])
print(experiments)

for i in experiments:
    #read GTF-experiment
    temp_1 = pd.read_csv('./'+ str(org) + '/'+ str(org) + "/" + str(i) + "_2.gtf", sep="\t", header=None)
    # Drop out exons
    temp_1 = temp_1[(temp_1[2] == "transcript")]
    # text filtering we need transcript_id and TPM
    temp_1["transcript"]= temp_1[8].str.split('; ', n=2, expand=True)[1].str.split(' ', n=2, expand=True)[1].str.replace('"', '')
    temp_1["TPM"]= temp_1[8].str.split('; ', n=5, expand=True)[4].str.split(' ', n=2, expand=True)[1].str.replace('"','').str.split(';', n=2, expand=True)[0]
    # In some cases we've got a refID so make a new column with new positon of TPM(TPM2), after replace the original TPM column with TPM2, where TPM2 is not null
    temp_1["TPM2"]= temp_1[8].str.split('; ', n=5, expand=True)[5].str.split('; ', n=5, expand=True)[2].str.split(' ', n=2, expand=True)[1].str.replace('"','').str.split(';', n=2, expand=True)[0]
    temp_1.loc[temp_1['TPM2'].notnull(), 'TPM'] = temp_1['TPM2']
    #it's ready
    temp_1 = temp_1[["transcript", "TPM"]]
    temp_1.shape
    
    #OK now to cuffcompare
    temp_2 = pd.read_csv('./'+ str(org) + '/compare/' + str(i) + "_vs_merged.combined.gtf", sep="\t", header=None)
    # We need ID, refID and classcode =
    temp_2["transcript"] = temp_2[8].str.split('; ', n=8, expand=True)[4].str.split(' ', n=2, expand=True)[1].str.replace('"', '')
    temp_2["ref"] = temp_2[8].str.split('; ', n=8, expand=True)[5].str.split(' ', n=2, expand=True)[1].str.replace('"', '')
    temp_2["classcode"]= temp_2[8].str.split('; ', n=8, expand=True)[6].str.split(' ', n=2, expand=True)[1].str.replace('"', '')
    temp_2 = temp_2[["transcript","ref","classcode"]]
    # Filter out classcodes not =
    temp_2 = temp_2[(temp_2["classcode"] == "=")]
    # remove duplicates cased by exons
    temp_2 = temp_2.drop_duplicates()
    temp_2.shape
    
    #make a temp_2 with TPMs :/
    new_df = pd.merge(temp_2, temp_1,  how='left', on="transcript")
    #remove trash
    new_df = new_df.drop(['transcript', 'classcode'], axis=1)
    # make a new names for reference and TPM(now will be lib_ID)
    new_df.columns = ["transcript", str(i)]
    new_df.shape
    
    #And finally
    main_df = pd.merge(main_df, new_df,  how='left',  on="transcript")


# All NaNs to 0 and finally drop the scaffolds
main_df = main_df.fillna(0)
main_df = main_df.drop([0], axis=1)
main_df.head()

#last check with our final lnc_rna set and if we are recalculating everything lets optimize a bit and drop transcripts we don't need
michal_to_alex = {
    "microcebus" : "Lemur",
    "pan" : "Chimp",
    "atys" : "Atys",
    "fascicularis" : "MacacaFascicularis",
    "gorilla" : "Gorilla",
    "paniscus" :"Bonobo",
    "papio" : "Baboon",
    "pongo" : "Orangutan",
    "marmoset" : "Marmoset",
    "sabaeus" : "GreenMonkey",
    "macaca" : "Rhesus"}
#
check = pd.read_csv('./check/'+ str(michal_to_alex.get(str(org))) + '/' + str(michal_to_alex.get(str(org))) + '_lncrnas_data_filtered.txt', sep="\t", skiprows=1, names = list(range(0,50)))
check.shape

check= check[[1]]
check.columns = ['transcript']
check = pd.merge(check, main_df,  how='left',  on="transcript")

print("Check shape: ",check.shape)

check.head()

check.describe()

check = check.drop_duplicates()
print("Check shape with expression: ", check.shape)
check.to_csv('./'+ str(org) + '/'+ str(michal_to_alex.get(str(org))) + "_SRX_lncRNAs_short.txt", index=False)
