import os
import sys
org=sys.argv[1]

f1 = open('./'+ str(org) + '/GTFs.txt') # a list of GTFs

GTFs = []

for line in f1:
  GTFs.append(line.strip().split('/')[3])


for GTF in GTFs:
  print GTF
  cmd = "cuffcompare -r ./" + str(org) + "/merged.gtf -R -o ./" + str(org) + "/compare/" + GTF.replace('_2.gtf', '') + "_vs_merged -C -G ./" + str(org) + "/" + str(org) + "/" + GTF
  print(cmd)
  os.system(cmd)

print "Done"
