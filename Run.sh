#!/bin/bash

species=(microcebus pan atys fascicularis gorilla paniscus papio pongo marmoset sabaeus macaca)

for i in ${species[@]}; do
	org=$i
	echo $org
	rm -rf ./$org/
	fold=_filtered_merged
	lncdata=_lncrnas_data.txt
	mkdir ./$org
	mkdir ./$org/compare
	echo "make a copy of data"
  	cp ~/primates/primate_lncrnas/$org$fold/merged.gtf ./$org
	cp ~/primates/primate_lncrnas/PREDICTIONS_2/$org/$org$lncdata ./$org
	cp -R ~/primates/primate_lncrnas/$org ./$org
	echo "list of gtfs"
	ls ./$org/$org/SRR*_2.gtf > ./$org/GTFs.txt # all GTFs for your species
	echo "compare py"
	python3 ./expression.py $org
	rm -rf ./$org/
done
exit 0
